# About

Just my simple personal page using the `startbootstrap-freelancer` template.

# Template

https://startbootstrap.com/template-overviews/freelancer

It's a jekyll site and the source code can be found at [GitHub](https://github.com/BlackrockDigital/startbootstrap-freelancer).

# License

MIT License
